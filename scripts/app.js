// Load the prompt module. 
var prompt = require('prompt');
prompt.start();

userPrompt(); 

function userPrompt() {
  prompt.get(['command'], function(error, input) { 
    if (error) {
      return onErr(error);
    }
    else {
      if (input.command == "safeRoute") {
        sendSMS();
      }
    }
  });
}

function sendSMS() {
  //  Load the twilio module. 
  var twilio = require('twilio');
 
  //  Create a new REST API client to make authenticated requests against the
  //  twilio back end.
  var client = new twilio.RestClient('AC56c00fc8faea87c82b8ecdd08f7c13e9', 'cff369aa64dd3b0fc066741ff2513ca5');
 
  //  Pass in parameters to the REST API using an object literal notation. The
  //  REST client will handle authentication and response serialzation for you.
  client.sms.messages.create({
    to: '+17188255454',
    from: '+13478366788', 
    body: 'Will be some link'
  }, function(error, message) {
    //  The HTTP request to Twilio will run asynchronously. This callback
    //  function will be called when a response is received from Twilio
    //  The "error" variable will contain error information, if any.
    //  If the request was successful, this value will be "falsy".
    if (!error) {
      //  The second argument to the callback will contain the information
      //  sent back by Twilio for the request. In this case, it is the
      //  information about the text messsage you just sent:
      console.log('Success! The SID for this SMS message is:');
      console.log(message.sid);
 
      console.log('Message sent on:');
      console.log(message.dateCreated);
    }
    else {
      console.log('Oops! There was an error.');
    }
  });
}

function onError(errorParam) {
  console.log(errorParam);
  return 1; 
}
